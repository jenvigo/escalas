<?php
namespace App;

class Escalas
{

    public static $notas_arr = [
        "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#",
        "A", "A#", "B"
    ];

    public static function string($nota_del_string)
    {
        global $i;
        global $semitonos;
//        global $notas_arr;

        $key_nota_string_arr = array_keys(Escalas::$notas_arr,
            $nota_del_string);
        $key_nota_string = $key_nota_string_arr[0];
//        $key_nota_string = $nota_del_string;


        $arr_semitonos = range(0, $semitonos);
        foreach ($arr_semitonos as $index => $semitono) {

            ?>
            <td><?= (self::$notas_arr[$key_nota_string]) ?? "";
                ?></td>
            <?
            self::add_one_or_reset_index_if_counter_is_in_the_last_element
            ($key_nota_string, Escalas::$notas_arr);

        }

    }

    public static function add_one_or_reset_index_if_counter_is_in_the_last_element
    ($key_nota_string, $notas_arr)
    {
        if ($key_nota_string === array_key_last($notas_arr)) {
            //LAST ELEMENT and reset index
            $key_nota_string = 0;
        } else {
            $key_nota_string++;
        }
        return $key_nota_string;
    }

    public static function fretboard()
    {
        global $semitonos;
        $arr_notas_strings = [
            "E",
            "A",
            "D",
            "G",
            "B",
            "E"
        ];
        $semitonos = 12;

        ?>
        <table border="1" class="fretboard table">
            <thead class="thead-dark">
            <?
            Escalas::tr_num_trastes();
            ?>
            </thead>
            <tbody>
            <?
            foreach ($arr_notas_strings as $nota_del_string) {
                ?>
                <tr>
                    <?
                    Escalas::string($nota_del_string);
                    ?>
                </tr>
                <?
            }
            ?>
            </tbody>
        </table>
        <style>
            .fretboard td:hover {
                background: black;
                color: white;
            }
        </style>
        <?
    }

    private static function tr_num_trastes()
    {
        global $semitonos;
        ?>
        <tr>
            <?
            for ($i = 0; $i <= $semitonos; $i++) {
                ?>
                <th><?= $i; ?></th>
                <?
            }
            ?>
        </tr>
        <?
    }

    public static function get_note_from_another_and_interval
    ($note, $interval)
    {
        do {
            $index =
                Escalas::add_one_or_reset_index_if_counter_is_in_the_last_element
                ($note, Escalas::$notas_arr);
            $interval--;
        } while ($interval != 0);

        $nota_arr_selected = (Escalas::$notas_arr[$index ??
            null]??null);
        return $nota_arr_selected ?? null;
    }

}