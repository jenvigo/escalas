<?php
namespace App;

class Selector
{
    public static function current_note()
    {
        ?>
        <div id="current_note">

        </div>
        <style>
            #current_note {
                width: 100px;
                height: 100px;
                border: 1px solid black;
                font-size: 4rem;
                text-align: center;
            }
        </style>
        <?
    }

    public static function scales_dropdown()
    {
        $scales_arr = [
            "Escala mayor" => array("2", "1", "1", "2"),
            "Escala menor" => array("2", "1")
        ];

        ?>

        <select name="scale" id="scale" class="form-control">
            <!--            <option value=""></option>-->
            <?
            foreach ($scales_arr as $scale => $pattern) {
                ?>
                <option value=""><?= $scale; ?></option>
                <?
            }
            ?>
        </select>

        <h3 class="mt-3">Key selected!!: <?= $key_scale = "E";?></h3>
        <h3 class="mt-3">
            <?
            $note = $key_scale;
            foreach ($scales_arr["Escala mayor"] as $interval){

                echo $note;

                echo " - ";

                $note = Escalas::get_note_from_another_and_interval
                ($note , $interval);
            }
            ?>

        </h3>
        <h3 class="pattern mt-3">
            <?=
            implode(" - ", $scales_arr["Escala mayor"]);
            ?>
        </h3>
        <?
    }
}