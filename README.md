# Escalas
Mi intención con este git es crear una herramienta para memorizar 
escalas que sirva a todos aquellos que estén iniciandose al solfeo. 

Me gustaría que este fuera un proyecto abierto a quien quiera 
mejorarlo, así que cualquier aportación será bien recibida. 

Por mi parte, intentaré ir dedicándole tiempo cuando me sea posible. 

