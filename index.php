<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//include_once $_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php';
include_once 'vendor/autoload.php';
//include_once '/vendor/autoload.php';
//include_once 'src/Escalas.php';
include_once 'src/Selector.php';
include_once 'dependencies.php';
use App\Escalas;
use App\Selector;
?>
<body>
<div class="container">
    <div class="d-flex">
        <?
        Escalas::fretboard();
        Selector::current_note();
        ?>
    </div>
    <?
    Selector::scales_dropdown();
    ?>
</div>
<script src="js/nota-select.js"></script>
</body>